extern crate codewars;

use codewars::exercises::sjf;
use codewars::exercises::lightsabers;
use codewars::exercises::order;
use codewars::exercises::square_sum;
use codewars::exercises::add_args;

#[test]
fn returns_expected() {
    assert_eq!(sjf::sjf(&[100], 0), 100);
    assert_eq!(sjf::sjf(&[3,10,20,1,2], 0), 6);
    assert_eq!(sjf::sjf(&[10,10,10,10,10], 2), 30);
    assert_eq!(sjf::sjf(&[1,10,10,10,2], 2), 23);
}

#[test]
fn lightsabers_test() {
//    let num = ToString(1);
    assert_eq!(lightsabers::how_many_lightsabers_do_you_own("blah"), 0);
    assert_eq!(lightsabers::how_many_lightsabers_do_you_own("Zach"), 18);
}

#[test]
fn order_test() {
//    assert_eq!(order("is2 Thi1s T4est 3a"), "Thi1s is2 3a T4est");
//    assert_eq!(order(""), "");
}

#[test]
fn square_sum_test() {
    assert_eq!(square_sum::square_sum(vec![1, 2]), 5);
    assert_eq!(square_sum::square_sum(vec![-1, -2]), 5);
    assert_eq!(square_sum::square_sum(vec![5, 3, 4]), 50);
}


#[test]
fn basic_tests() {
    assert_eq!(add_args::add(&[]), 0);
    assert_eq!(add_args::add(&[4,-3,-2]), -8);
}