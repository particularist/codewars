pub fn add(args: &[i64]) -> i64 {
    let pairs = args.iter().enumerate();
    println!("{:?}", pairs);
    pairs.map(|(index, &value)| (index as i64 + 1) * (value as i64)).fold(0, |acc, len| acc + len)
}