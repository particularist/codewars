pub fn how_many_lightsabers_do_you_own(name: &str) -> u8 {
    let lightsaber_count = match name.as_ref() {
        "Zach" => 18,
        _ => 0
    };
    lightsaber_count
}