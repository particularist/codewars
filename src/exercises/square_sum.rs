pub fn square_sum(vec: Vec<i32>) -> i32 {
   vec.iter().map(|&value| value * value).fold(0, |acc, len| acc + len)
}