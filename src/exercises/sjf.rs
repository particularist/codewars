
pub fn sjf(jobs: &[usize], index: usize) -> usize {
    let smaller_job_cycles = jobs.into_iter().filter(|&&x| x < jobs[index]).fold(0, |sum, i| sum + i);
    let equal_job_cycles = jobs.into_iter().take(index + 1).filter(|&&x| x == jobs[index]).fold(0, |sum, i| sum + i);
    smaller_job_cycles + equal_job_cycles
}
