pub mod exercises {
    pub mod sjf;
    pub mod lightsabers;
    pub mod order;
    pub mod square_sum;
    pub mod add_args;
}