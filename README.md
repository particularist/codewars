    ..%%%%....%%%%...%%%%%...%%%%%%..%%...%%...%%%%...%%%%%....%%%%.....%%...
    .%%..%%..%%..%%..%%..%%..%%......%%...%%..%%..%%..%%..%%..%%........%%...
    .%%......%%..%%..%%..%%..%%%%....%%.%.%%..%%%%%%..%%%%%....%%%%.....%%...
    .%%..%%..%%..%%..%%..%%..%%......%%%%%%%..%%..%%..%%..%%......%%.........
    ..%%%%....%%%%...%%%%%...%%%%%%...%%.%%...%%..%%..%%..%%...%%%%.....%%...
    .........................................................................

This repo is home to Rust exercises I've completed (or near completed) on codewars.com.

## Building

I used cargo... so.
`cargo build`

## Adding an exercise class!

1. Add the new file in the exercises directory.
1. Add your implementation function to the exercise.
1. Modify the mod.rs file to expose the implementation.
1. Add the new file in the lib.rs directory. 
1. Add the test function and appropriate import.

## Testing

Currently I'm accumulating tests in main_test.rs. 

To run the tests:
`cargo test`
